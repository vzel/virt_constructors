// Compile: g++ main.cpp -std=c++11 -g -ldl -export-dynamic
// Usage: ./a.out

#include <iostream>
#include <stdlib.h>
#include <cstdio>
#include <dlfcn.h>
/// The problem:
///           Input: 0, 1, 2 integer number
///           Output: in-memory object of class Derived0, Derived1, Derived2
///           Conditions: do not dublicate information, it is already clear
///           that Derived1 is associated with the number '1',
///           no switches for taht
/// Overview: There are a lot of ways to implement 'virtual constructors':
///           Abstract fabrics, clone functions, etc.
///           All these mechanisms use the same techniques: they read
///           already defined information (like '1' -> Dervied1) and
///           allocate place on the heap.
///           Search can be implemented as an efficient
///           unordered map (red black tree), but if such information is
///           very huge, it is dublication,
///           because a programmer should anyway define that
///           Derived1 is referenced to '1' user's input value.
/// Message:  It is expected, that the class can be fully described
///           by itself and it's base class definitions, because of
///           reflexion on the level of symbol table.
///           Possible syntax:
///           int classnumber;
///           std::cin >> classnumber;
///           NewClass_classnumber object;
///                    ^
///                    |---- it is 'inlined' from programmer's
///                          point of view
///           So no need to write switched based on 'classnumber',
///           The following implementation describes the details.

/// Target researched function specification
typedef void foo_type(void);

/// Description: Some base class that provides an interface
///              function named foo();
struct Base {
    virtual void foo() {
        std::cout << "Base::foo()" << std::endl;
    }
};

/// Description: A set of three derived classes that
///              implement function from the base;

/// Important note: below we can see the place where
///                 functions Derived0::foo, Dervied1::foo, etc
///                 are defined. After these lines writtern,
///                 the compiler will create symbols for these function.
struct Derived0 : public Base {
    void foo() {
        std::cout << "Derived0::foo()" << std::endl;
    }
};
struct Derived1 : public Base {
    void foo() {
        std::cout << "Derived1::foo()" << std::endl;
    }
};
struct Derived2 : public Base {
    void foo() {
        std::cout << "Derived2::foo()" << std::endl;
    }
};

/// Description: Default way to implement 'virtual constructors',
///              'clone' functions, abstract fabrics, etc.
Base * FakeVirtual_AbstractFabric(int num) {
    switch(num) {
        /// Note: information is dublicated,
        ///       Derived0::foo() place needed for
        ///       Dervied1::Dervied1() call is already
        ///       stored in the code above (see important note)
        case 0: return new Derived0;
        case 1: return new Derived1;
        case 2: return new Derived2;
        default: return nullptr;
    }
}

/// Description: an efficient way to create object virtually,
///              use direct function's places to create an object
Base * VirtualConstructor_direct(int num) {
    /// If we need to create the 'third' object,
    /// just take the needed function from the table
    /// as they are already stored. No need to specify,
    /// that 'third' function can be found by going through switch
    /// at the position of 3.
    const std::string vfunc_sym_name
        = "_ZN8Derived" + std::to_string(num) + "3fooEv";

    void * hdl;
    hdl = dlopen(NULL, 0);
    void * ptr = dlsym(hdl, vfunc_sym_name.c_str());

    size_t taget_vfunc_storage = (size_t) ptr;

    // No switch at all.
    foo_type* virtual_function =
        (foo_type*) (taget_vfunc_storage);

    /// Establish virtual table, create a basis
    /// for a further derived object,
    /// inheriting all base's fields
    void * scratch = (void *) new Base;
    void ** vt = (void **) malloc(sizeof(void*));
    *vt = (void *) virtual_function;
    *(void**)scratch = (void *) vt;
    return (Base*) scratch;
}

/// Note: Below is the sequentially predefined implementation.
///       If previous method works for any class'es names, this involves
///       C-style dereferencing of symbol table.
/// Description: an efficient way to create object virtually,
///              use the symbol table to create an object in runtime
///              by the offset from the classes' function storage beginning
Base * VirtualConstructor_offset(int num) {
    /// Note: "_ZN8Derived03fooEv" string should be
    ///       evaluated by the compiler according to
    ///       function translation rules, e.g. 'from C++ to raw C'
    ///       (example: C++ Class::f() -> ASM/C Class_f_func())
    const std::string vfunc_sym_name
        = "_ZN8Derived03fooEv";

    void * hdl;
    hdl = dlopen(NULL, 0);
    void * ptr = dlsym(hdl, vfunc_sym_name.c_str());

    size_t vfuncs_storage_begin = (size_t) ptr;
    /// Note: the step that function foo0, foo1, foo2 are placed in the mem,
    ///       can be evaluated by the compiler or linker if possible
    size_t vfuncs_step = 0x2C;
    /// Common solution:
    /// 1. Check that foo0, foo1, foo2 are sequential
    /// 2. Determine an offset for each of them individually

    // No switch at all, no information dublication
    foo_type* virtual_function =
        (foo_type*) (vfuncs_storage_begin + num * vfuncs_step);

    /// Create a basis for a further derived object,
    /// inheriting all base's fields
    void * scratch = (void *) new Base;
    void ** vt = (void **) malloc(sizeof(void*));
    *vt = (void *) virtual_function;
    *(void**)scratch = (void *) vt;
    return (Base*) scratch;
}

int main() {
    /// Program output expectation: real virtual constructors
    /// produces the same objects as so called 'abstract' fabrics

    Base * non_virtual_object
        = FakeVirtual_AbstractFabric(0); // switch
    non_virtual_object->foo();
    // Output: Derived0::foo()

    Base * virtual_object
        = VirtualConstructor_offset(1); // direct virtual call
    virtual_object->foo();
    // Output: Derived1::foo()

    Base * virtual_object_2
        = VirtualConstructor_direct(2); // calculation of the address and call
    virtual_object_2->foo();
    // Output: Derived2::foo()
}
